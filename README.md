# SWE Brige Example Project #
This project shows how to use the [SWE Bridge](https://bitbucket.org/swebridgedevelopment/swebridge) as a submodule. It contains two simple examples, a simple swe bridge setup in an external project and an example on how to use it in a multi-thread environment. 

## Building Info ##
To build this project in [Eclipse C/C++ IDE](https://www.eclipse.org/downloads/packages/):

1. **Clone this repository with submodules**
> $ git clone --recurse-submodules https://bitbucket.org/swebridgedevelopment/swebridge

3. **Add library paths** in eclipse project: properties -> c/c++ general -> paths and symbols -> includes:
> /${ProjName}/swebridge

4. **Link libraries ** Open the Libraries tab in properties -> c/c++ general -> paths and symbols -> libraries and add the follwing libraries to the linker:
> rt  
> m  
> pthread (if multithreading is used)  

5. **Disable SWE Bridge Standalone mode**: open "swebridge/platform_config.h" an change to OFF the option SWE_BRIDGE_STANDALONE 

6. **Build project**

