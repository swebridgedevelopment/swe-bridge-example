/*
 *  This File contains an example on how to use the SWE Bridge.
 *  It contains a simple standalone SWE Bridge example and an
 *  example with threading.
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#include "swe_bridge.h"
}
#else
#include "swe_bridge.h"
#endif


/*
 * Select one mode of opearation (only one!):
 *
 *   1. SWE_BRIDGE_SIMPLE_SETUP: SWE Bridge is used as a simple standalone application
 *
 *   2. SWE_BRIDGE_THREADING_ENVIRONMENT: An example on how to use the SWE Bridge with
 *      threads, pausing and resuming the scheduler
 *
 */
#define SWE_BRIDGE_SIMPLE_SETUP ON
#define SWE_BRIDGE_THREADING_ENVIRONMENT OFF


#if SWE_BRIDGE_SIMPLE_SETUP == ON
/*
 * Example main that calls the SWE Bridge
 */
int main(int argc, char** argv) {
	if (swe_bridge_setup(argc, argv) != swe_ok) {
		errmsg("Couldn't setup SWE Brige");
		exit_platform();
	}
	swe_bridge_init();
	swe_bridge_start();
	swe_bridge_exit();
	exit_platform();
	return 0;
}



#elif  SWE_BRIDGE_THREADING_ENVIRONMENT == ON
/*
 * Example on how to use the SWE Bridge with threads. A thread will
 * pause and resume the SWE Bridge at different intervals and finally
 * stop it.
 *
 * WARNING: pthread needs to included in the linker!
 */

#include <pthread.h>
#include <unistd.h>


/*
 * Convert the SWE Bridge status to text
 */
const char* status_text[]= {
		"swe_bridge_inital_state", // Initial state
		"system_check",            	 // Performing a system check
		"writing_puck_memory",    	 // Writing a SensorML file to a PUCK memory
		"extracting_puck_memory",  	 // Extracting a SensorML file from a PUCK memory
		"decoding_sensorml_file", 	 // Decoding & Parsing a SensorML file
		"swe_bridge_scheduler_setup",  // Setting up the scheduler
		"swe_bridge_scheduler_init",   // Executing scheduler init processes
		"swe_bridge_scheduler_running",// Scheduler is running
		"swe_bridge_scheduler_paused", // Scheduler is paused
		"swe_bridge_scheduler_stop"    // Scheduler is stopped
		"swe_bridge_scheduler_deep sleep"    // Scheduler is stopped
};

/*
 * Wait n seconds
 */
void wait(int seconds) {
	ulong last_time = time(NULL);
	dmsg("waiting %d seconds...", seconds);
	while (time(NULL) < last_time + seconds ) {
		usleep(10000);
	}
}


/*
 * Pauses and resumes the SWE Bridge each 5 seconds
 */
void* pause_resume_bridge(void* arg) {
	swe_bridge_status status;
	int i, j;
	int delay = 5;
	for ( i=0 ; i<10 ; i++ ) {
		wait(1);
		for ( j = 0 ; j < delay ; j++ ) {
			status = swe_bridge_get_status();
			cimsg(WHT, "			====> Status: \"%s\"", status_text[(int)status]);
			wait(1);
		}
		cimsg(WHT, "			====> Pausing Scheduler");
		swe_bridge_pause();
		if (i == 3 ){
			cimsg(WHT, "			====> Reset Scheduler");
			swe_bridge_reset();
		}
		for ( j = 0 ; j < delay ; j++ ) {
			status = swe_bridge_get_status();
			cimsg(WHT, "			====> Status: \"%s\"", status_text[(int)status]);
			wait(1);
		}
		cimsg(WHT, "			====> Start Scheduler");
		swe_bridge_resume();
	}
	return NULL;
}


/*
 * Example main launching the SWE Bridge and creating
 * a thread that will pause and resume the scheduler
 */
int main(int argc, char** argv) {
	pthread_t tid;
	if (swe_bridge_setup(argc, argv) != swe_ok) {
		errmsg("Couldn't setup SWE Brige");
	}
	// Launch a thread that will pause and resume the SWE Bridge
	pthread_create(&(tid), NULL, &pause_resume_bridge, NULL);
	swe_bridge_init(); // Initialize the SWE Bridge
	swe_bridge_start(); // Launch the Scheduler
	return 0;
}



#endif



